import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { MyApp} from './app.component';
import{FIREBASE_CONFIG} from './app.firebase.config';
import {AngularFireModule} from 'angularfire2';
import {AngularFireAuthModule}from 'angularfire2/auth';
import { SignPage } from '../pages/sign/sign';
import { RegisterPage} from '../pages/register/register';
import { HomePage } from '../pages/home/home';
import { AddpostPage } from '../pages/addpost/addpost';
import{ AngularFireDatabase } from 'angularfire2/database'
import { AngularFireDatabaseModule } from 'angularfire2/database';
@NgModule({
  declarations: [
    MyApp,
    SignPage,
    RegisterPage,
    HomePage,
    AddpostPage

  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    AngularFireModule.initializeApp(FIREBASE_CONFIG),
    AngularFireAuthModule,
    AngularFireDatabaseModule// imports firebase/database, only needed for database features

    
    
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    SignPage,
    RegisterPage,
    HomePage,
    AddpostPage

  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
