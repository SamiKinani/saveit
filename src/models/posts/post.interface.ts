export interface Posts{
    username:String;
    postid:Number;
    postDetails:String;
    item:String;
    phone:Number;
    street:String;
    city:String;
    long:String;
    lang:String;
    postDate:Date;
    endDate:Date;
    picture:String;
}