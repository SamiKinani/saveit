import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { AngularFireAuth } from 'angularfire2/auth';
import{ AngularFireDatabase } from 'angularfire2/database'
import { Posts } from '../../models/posts/post.interface';
import { FirebaseListObservable} from 'anguarfire2/database'
/**
 * Generated class for the AddpostPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-addpost',
  templateUrl: 'addpost.html',
})
export class AddpostPage {
  post = {} as Posts;
  postRef$:FirebaseListObservable<Posts[]>;

  constructor(public database :AngularFireDatabase,private alertCtrl:AlertController,
    public navCtrl: NavController, public navParams: NavParams) {
      this.postRef$=this.database.app.list('posts-list')
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddpostPage');
  }

}
