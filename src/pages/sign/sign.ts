import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { User } from "../../models/user";
import { RegisterPage } from '../register/register';
import {AngularFireAuth} from 'angularfire2/auth';
import { HomePage } from '../home/home';
@IonicPage()
@Component({
  selector: 'page-sign',
  templateUrl: 'sign.html',
})
export class SignPage {

  
  user = {} as User;
  constructor(private afAuth: AngularFireAuth,
    public navCtrl: NavController, public navParams: NavParams) {
 }
   async login(user:User) {
     try{
const result = this.afAuth.auth.signInWithEmailAndPassword(user.email,user.password)
console.log(result);
     if(result)
     {
  this.navCtrl.setRoot(HomePage);
     }
}

  catch(e)
  {
    console.error(e);


  }
}
  registerfunc()
  {
this.navCtrl.push(RegisterPage);
}

}